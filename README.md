![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/alex-thiel/application/master)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues-raw/alex-thiel/application)

# Application #

Eine JAVA Bibliothek zur Vereinfachung der Erstellung von SWING basierten Anwendungen. Viele Funktionen, welche in allen GUI Anwendungen verwendet werden, sind in dieser Bibliothek bereits vorbereitet und implementiert.

### Warum diese Bibliothek? ###

Ich habe mit der Entwicklung bereits während meines Studiums begonnen, da bei Studienarbeiten immer wieder viel Zeit damit vergeudet wurde einige grundlegende Features von grafischen Anwendungen neu zu entwickeln.

Sicherlich kann man Code auch von einem Projekt zum nächsten weiter geben und kopieren. Allerdings ergibt sich hierdurch kein wirklicher Fortschritt und von neuen Ideen können ältere Entwicklungen nicht mehr profitieren.

Mit dem Beginn der Entwicklung zu dieser Bibliothek konnten viele der Basisfunktionen implementiert und weiter verbessert werden.

Ich würde mich freuen, wenn ich mit dieser Bibliothek auch anderen helfen kann und freue mich, wenn diese Bibliothek weiter entwickelt wird.

### Was ist in der Bibliothek? ###

Die Bibliothek bietet in der aktuellen Version folgende Funktionen:

* Umschaltung des Look-and-Feel auf alle im System verfügbaren Styles
* Splashscreen der einfach individuell angepasst werden kann
* Integration einer Online-Hilfe mittels Java-Help

### Verwendung ###

Die Erstellung einer eigenen Anwendung auf Basis dieser Bibliothek erfolgt nach dem folgenden Schema:

```java
package de.alx_development.MyApplication;

import de.alx_development.application.Bootstrapper;
import de.alx_development.application.JApplicationFrame;

/**
 * Die zu erstellende Anwendung muss das JApplicationFrame als
 * Basisklasse erweitern.
 */
public class MyApplication extends JApplicationFrame {

    /**
     * public static void main instantiiert den Bootstrapper und veranlasst
     * dass dieser die Anwendung auf Basis der erweiterten JApplicationFrame-
     * Klasse lädt.
     */
    public static void main(String[] args) {
        Bootstrapper.load(MyApplication.class);
    }

    /**
     * Die Implementierung der eigenen Anwendung
     */
    public MyApplication() {
        super();

        /*
         * Hier können grundlegende Einstellungen der Anwendung vorgenommen
         * werden, wie die Vergabe von Name, Version und Icon.
         */
        setTitle("MyApplication");
        setVersion("0.0.1");
        setApplicationIcon(new ImageIcon(MazeRunner.class.getResource("MyApplication.ICON.32x32.png")));
        
        //TODO: Implementiere hier deinen eigenen Code
    }
}
```



### Kontakt? ###

Eigene Ideen oder Bugs gefunden? Bitte meldet euch, ich freue mich über eure Mitarbeit.

* alex@thiel-online.net

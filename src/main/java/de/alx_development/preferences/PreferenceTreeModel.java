package de.alx_development.preferences;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2021 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * This class provides a simple tree model for handling
 * directory structures.
 *
 * @author Alexander Thiel
 */
public class PreferenceTreeModel implements TreeModel {
    /**
     * Internal logger instance to handle log messages in a unique way
     */
    private static final Logger logger = Logger.getLogger(PreferenceTreeModel.class.getName());
    /**
     * Root node which represents the central node of the model
     */
    private final Preferences userRoot;
    /**
     * List of listeners
     */
    protected EventListenerList listenerList = new EventListenerList();

    /**
     * Constructor without a filename filter and an
     * empty structure.
     */
    public PreferenceTreeModel() {
        // Setting up the root node
        userRoot = Preferences.userRoot();
    }

    /**
     * This method returns the child object as Object for the given parent
     * at the specified index.
     *
     * @param parent The parent node
     * @param index  Index of the child node
     */
    @Override
    public Preferences getChild(Object parent, int index) {
        try {
            Preferences f = (Preferences) parent;
            String childPath;

            if (parent.equals(getRoot()))
                childPath = String.format("/%s", f.childrenNames()[index]);
            else
                childPath = String.format("%s/%s", f.absolutePath(), f.childrenNames()[index]);

            if (f.nodeExists(childPath))
                return f.node(childPath);
            else
                return null;
        } catch (BackingStoreException | ClassCastException e) {
            logger.warning(e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Returns the number of children of the given parent node
     * object as integer value.
     *
     * @param node The node which children should be counted
     * @return Number of children
     */
    @Override
    public int getChildCount(Object node) {
        try {
            Preferences f = (Preferences) node;
            return f.childrenNames().length;
        } catch (BackingStoreException | ClassCastException e) {
            logger.warning(e.getLocalizedMessage());
            return 0;
        }
    }

    /**
     * This method may be used to determine which index the given
     * child has got in the parent node. If the given child object isn't
     * a child object of the parent node -1 is returned.
     *
     * @param parent The parent node
     * @param child  The searched child node
     * @return Index of the child between the parent
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        try {
            Preferences f = (Preferences) parent;
            // Convert String Array to List for better access to the strings
            List<String> list = Arrays.asList(f.childrenNames());
            return list.indexOf(child);
        } catch (BackingStoreException | ClassCastException e) {
            logger.warning(e.getLocalizedMessage());
            return -1;
        }
    }

    /**
     * This method returns the root node for the structure.
     *
     * @return The root node.
     */
    @Override
    public Object getRoot() {
        return userRoot;
    }

    /**
     * This method returns true, if the node is a leaf node.
     *
     * @param node The node which should be proofed
     * @return True, if the node is a leaf node
     */
    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    /**
     * This model isn't producing any events at present, so
     * this method is empty.
     *
     * @param treeModelListener The listener to add
     */
    public synchronized void addTreeModelListener(TreeModelListener treeModelListener) {
        listenerList.add(TreeModelListener.class, treeModelListener);
    }

    /**
     * This model isn't producing any events at present, so
     * this method is empty.
     *
     * @param treeModelListener The listener which should be removed
     */
    public synchronized void removeTreeModelListener(TreeModelListener treeModelListener) {
        listenerList.remove(TreeModelListener.class, treeModelListener);
    }

    /**
     * Messaged when the user has altered the value for the item identified
     * by <code>path</code> to <code>newValue</code>.
     * If <code>newValue</code> signifies a truly new value
     * the model should post a <code>treeNodesChanged</code> event.
     *
     * @param path path to the node that the user has altered
     * @param newValue the new value from the TreeCellEditor
     */
    public void valueForPathChanged(TreePath path, Object newValue) {

    }
}

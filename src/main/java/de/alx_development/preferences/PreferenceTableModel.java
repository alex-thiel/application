package de.alx_development.preferences;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2021 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.languages.Translator;

import javax.swing.table.AbstractTableModel;
import java.text.MessageFormat;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * The <code>PreferenceTableModel</code> specifies the methods the
 * <code>JTable</code> will use to interrogate with the preferences data.
 */
class PreferenceTableModel extends AbstractTableModel {

    /**
     * Internal logger instance to handle log messages in a unique way
     */
    private static final Logger logger = Logger.getLogger(PreferenceTreeModel.class.getName());

    private static final String[] COLUMN_NAMES = {
            Translator.getInstance().getLocalizedString("PROPERTY"),
            Translator.getInstance().getLocalizedString("PROPERTY_VALUE")
    };

    private final Preferences preferences;

    PreferenceTableModel(Preferences preferences) {
        super();
        this.preferences = preferences;
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    public int getRowCount() {
        return getPreferenceKeys().length;
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    /**
     * Returns the name of the column at <code>columnIndex</code>.  This is used
     * to initialize the table's column header name.  Note: this name does
     * not need to be unique; two columns in a table can have the same name.
     *
     * @param columnIndex the index of the column
     * @return the name of the column
     */
    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        String[] keys = getPreferenceKeys();
        String value = null;

        switch (columnIndex) {
            case 0:
                value = keys[rowIndex];
                break;
            case 1:
                value = preferences.get(keys[rowIndex], "N/A");
                break;
        }

        return value;
    }

    private String[] getPreferenceKeys() {
        String[] keys = new String[0];
        try {
            keys = preferences.keys();
        } catch (BackingStoreException e) {
            logger.warning(e.getLocalizedMessage());
        }
        return keys;
    }

    /**
     * Returns true if the cell at rowIndex and columnIndex is editable. Otherwise, setValueAt on the cell
     * will not change the value of that cell.
     *
     * @param rowIndex    the row whose value to be queried
     * @param columnIndex the column whose value to be queried
     * @return true if the cell is editable
     * @see #setValueAt(Object, int, int)
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1;
    }

    /**
     * Sets the value in the cell at columnIndex and rowIndex to aValue.
     *
     * @param aValue      the new value
     * @param rowIndex    the row whose value is to be changed
     * @param columnIndex the column whose value is to be changed
     * @see #getValueAt(int, int)
     * @see #isCellEditable(int, int)
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        assert (aValue instanceof String);

        if (columnIndex == 1) {
            preferences.put(getPreferenceKeys()[rowIndex], aValue.toString());
            logger.config(MessageFormat.format("Preference changed: \"{0}\" ->{1}", getPreferenceKeys()[rowIndex], aValue.toString()));
        }
    }
}

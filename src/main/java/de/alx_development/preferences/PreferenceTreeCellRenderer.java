package de.alx_development.preferences;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2021 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.images.ImageLoader;
import de.alx_development.filesystem.DirectoryCellRenderer;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.util.prefs.Preferences;

/**
 * PreferenceTreeCellRenderer should be used to get a nice styled
 * view for the preference tree view.
 *
 * @author Alexander Thiel
 */
public class PreferenceTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final ImageIcon closed_folder = new ImageIcon(DirectoryCellRenderer.class.getResource("closed_folder.gif"));
    private static final ImageIcon open_folder = new ImageIcon(DirectoryCellRenderer.class.getResource("open_folder.gif"));
    private static final ImageIcon preferencesIcon = ImageLoader.getInstance().getImageIcon("preferences");

    /**
     * The constructor of the cell renderer
     */
    public PreferenceTreeCellRenderer() {
        super();
    }

    /**
     * This is the sole method of this object. It passes the
     * given parameters to the wrapped TreeCellRenderer.
     *
     * @param tree     JTree Object which should be formatted.
     * @param value    The node to format
     * @param selected True, if the node is selected
     * @param expanded True, if the node is expanded
     * @param leaf     True, if this is a leaf node.
     * @param row      The row number
     * @param hasFocus True, if the node has got the focus.
     * @return Returns a TreeCellRendererComponent
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        // The passed tree node value
        Preferences node = (Preferences) value;

        setLeafIcon(preferencesIcon);
        setOpenIcon(open_folder);
        setClosedIcon(closed_folder);

        setText(node.name());

        return this;
    }
}

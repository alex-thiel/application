/*
 * SplashScreen.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */

package de.alx_development.application;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.logging.SplashScreenLogHandler;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.PixelGrabber;
import java.net.URL;

/**
 * This class is used to display the splash screen
 * at application startup using the application,
 * or to display an information window.
 *
 * @author Alexander Thiel
 */
public class SplashScreen extends Window {

    private static final long serialVersionUID = 1L;
    private static final Image splashImage;
    private static SplashScreen instance;

    static {
        /*
         * If no splash image is provided by the users application,
         * the default alx splash image is shown.
         */
        URL splashURL = SplashScreen.class.getResource("/splash.jpg");
        if (splashURL == null) {
            splashURL = SplashScreen.class.getResource("images/defaultSplash.jpg");
        }
        splashImage = Toolkit.getDefaultToolkit().createImage(splashURL);
    }

    private final Label textField = new Label();
    private boolean paintCalled = false;

    private static final SplashScreen screen = SplashScreen.getInstance(new Frame());

    /**
     * This method creates the window at the center
     * of the monitor and display the contents.
     *
     * @param owner The owner of the splashscreen frame
     */
    private SplashScreen(Frame owner) {
        super(owner);
        setLayout(new BorderLayout());
        new SplashScreenLogHandler(this);
        Container imageContainer = new Container();

        /*
         * Loading the image using the media tracker
         */
        MediaTracker mt = new MediaTracker(imageContainer);
        mt.addImage(splashImage, 0);
        try {
            mt.waitForID(0);
        } catch (InterruptedException ie) {
            System.err.println(ie.getMessage());
        }

        /*
         * Analyzing the picture color to adjust the foreground
         * and background color for the text field. The text fields
         * background should be the same as the splash pictures and
         * the foreground should be the inverted color.
         */
        final int[] pixels = new int[1];
        PixelGrabber grabber = new PixelGrabber(splashImage, 0, 0, 1, 1, pixels, 0, 1);
        try {
            grabber.grabPixels();
            int pixel = pixels[0];
            int b_red = (pixel >> 16) & 0xff,
                    b_green = (pixel >> 8) & 0xff,
                    b_blue = (pixel) & 0xff;
            int f_red = ~(pixel >> 16) & 0xff,
                    f_green = ~(pixel >> 8) & 0xff,
                    f_blue = ~(pixel) & 0xff;
            textField.setBackground(new Color(b_red, b_green, b_blue));
            textField.setForeground(new Color(f_red, f_green, f_blue));
        } catch (InterruptedException exc) {
            /*
             * Setting default values, if we can't get access
             * to the picture data.
             */
            System.err.println(exc.getMessage());
            textField.setBackground(Color.white);
            textField.setForeground(Color.black);
        }

        /*
         * Initialising the subcomponents
         */
        add(textField, BorderLayout.SOUTH);
        add(imageContainer, BorderLayout.CENTER);
        pack();

        /*
         * center the frame on the screen
         */
        int imgWidth = splashImage.getWidth(imageContainer);
        int imgHeight = splashImage.getHeight(imageContainer) + textField.getHeight();
        setSize(imgWidth, imgHeight);
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenDim.width - imgWidth) / 2, (screenDim.height - imgHeight) / 2);

        MouseAdapter disposeOnClick = new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                /*
                 * This is to avoid application hangup,
                 * cause mouse clicks may happen before
                 * the screen is displayed.
                 */
                synchronized (SplashScreen.this) {
                    SplashScreen.this.paintCalled = true;
                    SplashScreen.this.notifyAll();
                }
                dispose();
            }
        };
        addMouseListener(disposeOnClick);
    }

    /**
     * With this method the <code>SplashScreen</code> can start only once.
     *
     * @param splashFrame The frame to display as splash screen
     * @return _instance
     */
    public static synchronized SplashScreen getInstance(Frame splashFrame) {
        if (instance == null)
            instance = new SplashScreen(splashFrame);

        return instance;
    }

    /**
     * This method creates the splash screen and display
     * it.
     *
     * @param appName The application name to be displayed
     * @return The frame containing the splash screen
     */
    public static SplashScreen splash(String appName) {

        screen.setText(appName);
        screen.toFront();
        screen.setVisible(true);

        /*
         * This is to wait until the splash screen has been
         * displayed to avoid to be overtaken by the launched
         * application on fast machines.
         */
        if (!EventQueue.isDispatchThread()) {
            synchronized (screen) {
                while (!screen.paintCalled) {
                    try {
                        screen.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        }
        return screen;
    }

    /**
     * Set the text which has to be displayed in the lower text field
     * beyond the splash image.
     *
     * @param text The text to display
     */
    public void setText(final String text) {
        textField.setText(text);
    }

    /**
     * This method is for repainting the screen
     *
     * @param g A Graphics object
     */
    public void update(Graphics g) {
        g.setColor(getForeground());
        paint(g);
    }

    /**
     * This method paints the image into the frame
     *
     * @param g A Graphics object
     */
    public void paint(Graphics g) {
        g.drawImage(splashImage, 0, 0, this);
        if (!paintCalled) {
            paintCalled = true;
            synchronized (this) {
                notifyAll();
            }
        }
    }
}

/*
 * application.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */

package de.alx_development.application;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 *
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 *
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.languages.Translator;

import javax.help.HelpSet;
import javax.help.JHelpContentViewer;
import java.awt.*;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used as main class for displaying
 * the splash screen and info screen and loading another
 * class using the main method of this class.
 * The class to load must be specified by command line
 * parameters.
 * <p>
 * The first parameter is the name of the class which
 * main method should be loaded. All other parameters
 * are passed as arguments to the class which is loaded.
 * <p>
 * If you're interested in using the properties capability of the
 * application, place a properties styled file named like the
 * package of your application concat <code>.properties</code> in
 * the same directory as the application. A file with the same name
 * placed in the user's home directory overwrites the attributes.
 * Using both, you're able to provide system wide settings and
 * user dependent settings at the same time.
 *
 * @author Alexander Thiel
 */

public class Application {
    /**
     * Initializing the logging mechanism
     */
    public static Logger logger = Logger.getLogger(Application.class.getName());
    private static HelpSet helpset;

    /**
     * Main method for this loader. The given arguments
     * are used to specify the class to load with this loader.
     * All other arguments are passed to the class which is loaded by
     * this class.
     *
     * @param args command line arguments
     * @deprecated Use the new Bootstrapper to load a JFrame with splash screen
     */
    @Deprecated
    public static void main(String[] args) {
        /*
         * Extracting the program to load from the arguments array.
         * Starting the SplashScreen
         */
        SplashScreen splashScreen = null;
        Class<?> appclass = null;

        /*
         * Trying to find the requested class
         */
        String applicationClassName = args[0];
        try {
            appclass = Class.forName(applicationClassName);
        } catch (ClassNotFoundException e) {
            logger.log(Level.SEVERE, "Unable to locate application class " + applicationClassName + " (ClassNotFoundException)");
        }

        // Removing the class name from the arguments array
        String newArgs[] = new String[args.length - 1];
        System.arraycopy(args, 1, newArgs, 0, args.length - 1);

        if (!GraphicsEnvironment.isHeadless()) {
            String s = MessageFormat.format(Translator.getInstance().getLocalizedString("LOADING"), new Object[]{applicationClassName});
            splashScreen = SplashScreen.splash(s);
        }

        //Launching the application with the given arguments.
        try {
            appclass
                    .getMethod("main", new Class[]{String[].class})
                    .invoke(null, new Object[]{newArgs});
        } catch (Throwable e) {
            logger.log(Level.SEVERE, "Failed to launch application: " + e.getLocalizedMessage());
            System.exit(10);
        }

        // Destroy SplashScreen
        splashScreen.dispose();
    }

    /**
     * This method can be used to get access to the translation database
     * for all programs in this archive.
     *
     * @param key The key for the translation
     * @return The localized string for the requested key
     * @see de.alx_development.application.languages.Translator
     * @deprecated Don't use this function anymore. Use the specialized <code>de.alx_development.application.languages.Translator</code> object instead.
     */
    @Deprecated
    public static String getLocalizedString(String key) {
        return Translator.getInstance().getLocalizedString(key);
    }

    /**
     * This method provides access to the applications help set for
     * the JavaHelp system.
     *
     * @return Returns the helpset.
     */
    public static HelpSet getHelpset() {
        /*
         * Loading HelpSet if not already done
         */
        if (helpset == null) {
            /*
             * Loading the HelpSet if a HelpSet file is present in
             * help/HelpSet.hs. Errors are ignored.
             */
            try {
                // TODO: HelpSet could not be loaded without exception
                //helpset = new javax.help.HelpSet(null, javax.help.HelpSet.findHelpSet(application.class.getClassLoader(), "help/HelpSet.hs"));
            } catch (Exception exc) {
                logger.log(Level.WARNING, "Unable to locate online help definition");
            }
        }

        return helpset;
    }

    /**
     * This method returns a <code>JHelpContentViewer</code> object displaying
     * the given id if available.
     *
     * @param id
     * @return <code>JHelpContentViewer</code> component
     */
    public static JHelpContentViewer getHelpContentViewer(String id) {
        try {
            JHelpContentViewer viewer = new JHelpContentViewer(getHelpset());
            viewer.setCurrentID(id);
            return viewer;
        } catch (Exception exc) {
            logger.log(Level.SEVERE, exc.getLocalizedMessage());
            return null;
        }

    }
}

package de.alx_development.application;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.prefs.Preferences;

public class JDialogFrame extends JDialog {

    /**
     * Automatic generated serial UID
     */
    private static final long serialVersionUID = -1124810823377680799L;

    /**
     * Private method to perform basic component tasks.
     *
     * @see javax.swing.JDialog#dialogInit()
     */
    protected void dialogInit() {
        super.dialogInit();

        // Loading look and feel definitions and other preferences
        Preferences preferences = Preferences.userNodeForPackage(getClass());
        setSize(preferences.getInt("SIZE_WIDTH", 500), preferences.getInt("SIZE_HEIGHT", 300));
        setLocation(preferences.getInt("POSITION_X", 100), preferences.getInt("POSITION_Y", 100));

        /*
         * Adding additional component listener to be informed about
         * window size and location changes which should be stored
         * in the properties.
         */
        addComponentListener(new ComponentListener() {

            public void componentResized(ComponentEvent event) {
                preferences.putInt("SIZE_HEIGHT", event.getComponent().getHeight());
                preferences.putInt("SIZE_WIDTH", event.getComponent().getWidth());
            }

            public void componentMoved(ComponentEvent event) {
                preferences.putInt("POSITION_X", event.getComponent().getX());
                preferences.putInt("POSITION_Y", event.getComponent().getY());
            }

            public void componentShown(ComponentEvent event) {
            }

            public void componentHidden(ComponentEvent event) {
            }
        });
    }
}

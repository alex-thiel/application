/*
 * JApplicationFrame.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */

package de.alx_development.application;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 *
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 *
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.images.ImageLoader;
import de.alx_development.application.languages.Translator;
import de.alx_development.preferences.PreferenceDialog;

import javax.help.HelpSet;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.prefs.Preferences;

/**
 * This Frame extends the standard JFrame and implements the
 * basic functions of each program frame. A menu bar with help
 * is generated, if a help set is found in <code>/help/HelpSet.hs</code>.
 *
 * @author alex
 */
public class JApplicationFrame extends JFrame implements RootPaneContainer, ContainerListener {
    private static final long serialVersionUID = 1L;

    private Container contentPane;
    private JDialog console;
    private String version = "";

    /**
     * Constructor without any parameters.
     *
     * @throws java.awt.HeadlessException Thrown when code that is dependent on a keyboard, display, or mouse is called in an environment that does not support a keyboard, display, or mouse.
     * @see #JApplicationFrame(String)
     * @see javax.swing.JFrame
     */
    public JApplicationFrame() throws HeadlessException {
        this(null);
    }

    /**
     * Constructor with parameter <code>title</code>.
     *
     * @param title The title string for the main window, If null, the classname will be used
     * @throws java.awt.HeadlessException Thrown when code that is dependent on a keyboard, display, or mouse is called in an environment that does not support a keyboard, display, or mouse.
     * @see #JApplicationFrame()
     * @see javax.swing.JFrame
     */
    public JApplicationFrame(String title) throws HeadlessException {
        super();
        if (title == null) title = this.getClass().getName();
        setTitle(title);
    }

    /**
     * Private method to perform basic component tasks for the
     * menus and the console output.
     *
     * @see javax.swing.JFrame#frameInit()
     */
    protected void frameInit() {
        super.frameInit();

        // The image loader is used to load the icons used by the frontend
        ImageLoader imageloader = ImageLoader.getInstance();

        // Preference manager for the components persistent preferences
        Preferences preferences = Preferences.userNodeForPackage(getClass());

        // Preparing the content pane as container with a standard BorderLayout as layout manager.
        contentPane = new Container();
        contentPane.setLayout(new BorderLayout());
        setContentPane(contentPane);

        // Loading look and feel definitions and other preferences
        setSize(preferences.getInt("SIZE_WIDTH", 800), preferences.getInt("SIZE_HEIGHT", 600));
        setLocation(preferences.getInt("POSITION_X", 50), preferences.getInt("POSITION_Y", 50));
        setLookAndFeel(preferences.get("LOOK_AND_FEEL", null));

        // Setting default closing method
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                exit(0);
            }
        });

        /*
         * Adding additional component listener to be informed about
         * window size and location changes which should be stored
         * in the properties.
         */
        addComponentListener(new ComponentListener() {

            /**
             * Storing the new dimension in the preferences
             * @param event The components change event
             */
            public void componentResized(ComponentEvent event) {
                preferences.putInt("SIZE_HEIGHT", event.getComponent().getHeight());
                preferences.putInt("SIZE_WIDTH", event.getComponent().getWidth());
            }

            public void componentMoved(ComponentEvent event) {
                preferences.putInt("POSITION_X", event.getComponent().getX());
                preferences.putInt("POSITION_Y", event.getComponent().getY());
            }

            /*
             * These events are ignored, cause the application window
             * should be displayed every time.
             */
            public void componentShown(ComponentEvent event) {
            }

            public void componentHidden(ComponentEvent event) {
            }
        });

        /*
         * Initializing the menu and assigning the localized
         * languages.
         */
        JMenu applicationMenu = new JMenu(Translator.getInstance().getLocalizedString("APPLICATION"));
        JMenu helpMenu = new JMenu(Translator.getInstance().getLocalizedString("HELP"));
        JMenu lafMenu = new JMenu(Translator.getInstance().getLocalizedString("LOOK_AND_FEEL"));

        JMenuItem configMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("CONFIG"), imageloader.getImageIcon("config"));
        JMenuItem consoleMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("SHOW_HIDE_CONSOLE"), imageloader.getImageIcon("terminal"));
        JMenuItem exitMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("EXIT"), imageloader.getImageIcon("exit"));

        JMenuItem infoMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("INFO"), imageloader.getImageIcon("info"));
        JMenuItem helpMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("HELP"), imageloader.getImageIcon("help"));

        /*
         * Generating the console panel which handles the
         * logging output.
         */
        try {
            console = new Console();

            consoleMenuItem.addActionListener(
                    e -> console.setVisible(!console.isVisible()));
        } catch (Exception e) {
            Application.logger.log(Level.WARNING, "Unable to initialize console output.");
        }

        /*
         * Adding action event handling to the integrated menu bar
         * buttons
         */
        exitMenuItem.addActionListener(
                e -> exit(0));

        infoMenuItem.addActionListener(
                e -> SplashScreen.splash(getTitle().concat(" ").concat(version)));

        configMenuItem.addActionListener(e -> {
            Window frame = new PreferenceDialog(this);
            //frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
            frame.setVisible(true);
        });

        ActionListener lafActionListener = e ->
                setLookAndFeel(e.getActionCommand());

        /*
         * Building a menu to provide look and feel
         * switching depending on which look and feels
         * are currently supported by the installed JVM
         */
        lafMenu.setIcon(imageloader.getImageIcon("laf"));

        for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
            ImageIcon lafImageIcon;
            String lafName = laf.getClassName().substring(laf.getClassName().lastIndexOf(".") + 1).replaceAll("LookAndFeel", "");

            /*
             * Try to load an ImageIcon for the LookNFeel.
             * If no icon can be found, a standard icon is
             * used.
             */
            lafImageIcon = imageloader.getImageIcon(lafName);
            if (lafImageIcon == null)
                lafImageIcon = imageloader.getImageIcon("defaultLAF");

            JMenuItem lafMenuItem = new JMenuItem(lafName, lafImageIcon);
            lafMenuItem.setActionCommand(laf.getClassName());
            lafMenuItem.addActionListener(lafActionListener);
            lafMenu.add(lafMenuItem);
        }

        /*
         * Building the menu structure
         */
        applicationMenu.add(configMenuItem);
        applicationMenu.add(lafMenu);
        applicationMenu.add(consoleMenuItem);
        applicationMenu.addSeparator();
        applicationMenu.add(exitMenuItem);

        helpMenu.add(infoMenuItem);

        JMenuBar jMenuBar = new JMenuBar();
        jMenuBar.add(applicationMenu);
        jMenuBar.add(helpMenu);
        setJMenuBar(jMenuBar);

        /*
         * Loading the help set if a help set file is present in
         * help/HelpSet.hs. Errors are ignored.
         */
        try {
            HelpSet hs = Application.getHelpset();
            if (hs != null) {
                helpMenuItem.addActionListener(new javax.help.CSH.DisplayHelpFromSource(hs.createHelpBroker()));
                helpMenu.add(helpMenuItem);
            }
        } catch (Exception exc) {
            Application.logger.log(Level.WARNING, "Help system could not be loaded");
        }

        /*
         * Setting the default layout for the GUI
         */
        UIManager.put("TableHeader.foreground", (new Color(99, 99, 124)));
        UIManager.put("TableHeader.font", (new JButton()).getFont());

        super.getContentPane().add(getStatusBar(), BorderLayout.SOUTH);
    }

    /**
     * This internal method is used to set the look and feel using
     * the laf managers class name as parameter.
     *
     * @param laf_classname The class name for the look and feel
     */
    protected void setLookAndFeel(String laf_classname) {
        Preferences preferences = Preferences.userNodeForPackage(getClass());
        try {
            UIManager.setLookAndFeel(laf_classname);
            SwingUtilities.updateComponentTreeUI(JApplicationFrame.this);
            preferences.put("LOOK_AND_FEEL", laf_classname);
            Application.logger.log(Level.INFO, "Look and Feel changed to " + laf_classname);
        } catch (Exception exc) {
            preferences.remove("LOOK_AND_FEEL");
            Application.logger.log(Level.CONFIG, "Unable to load Look and Feel, Using default. " + exc.getMessage());
        }
    }

    /**
     * This method returns the version string as defined
     * using <code>setVersion(String)</code>.
     *
     * @return The version as <code>String</code>
     * @see #setVersion
     */
    public String getVersion() {
        return version;
    }

    /**
     * This function may be used to modify the version string
     * displayed in the information panel.
     *
     * @param version The version string of the application
     * @see #getVersion
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * This method gives access to the text in the info row
     * on the bottom of the application window.
     *
     * @return The text as <code>String</code>
     * @see #setInfotext
     * @see #setInfoicon
     */
    @SuppressWarnings("unused")
    public String getInfotext() {
        return getStatusBar().getCenterText();
    }

    /**
     * This method gives access to the text of the info row on the bottom
     * of the application window.
     *
     * @param info String representation of the Infotext.
     * @see #getInfotext
     * @see #setInfoicon
     */
    @SuppressWarnings("unused")
    public void setInfotext(String info) {
        if (info == null) {
            info = " ";
        }
        getStatusBar().setCenterText(info);
    }

    /**
     * This method should be used to set an icon in front
     * of the infotext in the info row.
     *
     * @param icon The <code>ImageIcon</code> object defining the icon to bew displayed in the status bar
     * @see #getInfotext
     * @see #setInfotext
     */
    @SuppressWarnings("unused")
    public void setInfoicon(ImageIcon icon) {
        getStatusBar().setIcon(icon);
    }

    /**
     * Returns the <code>contentPane</code> object for this frame.
     *
     * @return the <code>contentPane</code> property
     * @see #setContentPane
     */
    public Container getContentPane() {
        return contentPane;
    }

    /**
     * You may set your own ContentPane using this
     * method. This functionality is added to be able to use the
     * <code>JExtendedDesktopPane</code> as ContentPane.
     *
     * @param newPane A <code>Container</code> used as ContentPane
     * @see #getContentPane
     */
    public void setContentPane(Container newPane) {
        /*
         * Removing the old contentPane from the frame
         */
        contentPane.removeContainerListener(this);
        super.getContentPane().remove(contentPane);

        contentPane = newPane;

        /*
         * Setting the parameters for the new content pane.
         */
        contentPane.getToolkit().setDynamicLayout(true);
        contentPane.addContainerListener(this);
        super.getContentPane().add(contentPane, BorderLayout.CENTER);

        contentPane.repaint();
        super.getContentPane().repaint();
    }

    /**
     * Use this method to add a  additional menu item to the
     * menu bar. The menu item is added as last element before
     * the help menu.
     *
     * @param menuItem The menu to be added to the menu structure
     */
    @SuppressWarnings("unused")
    public void addMenu(JMenu menuItem) {
        /*
         * The last menu item is the help menu item.
         * If not, the new one is inserted without reorganizing
         * the menu.
         */
        JMenuBar jMenuBar = getJMenuBar();
        JMenu helpMenu = jMenuBar.getMenu(jMenuBar.getMenuCount() - 1);
        if (helpMenu.getName().equals("HELP")) {
            jMenuBar.remove(helpMenu);
            jMenuBar.add(menuItem);
            jMenuBar.add(helpMenu);
        } else {
            jMenuBar.add(menuItem);
        }
    }

    /**
     * This method is implemented by the ContainerListener and used to redraw
     * the container content if this has been changed.
     *
     * @param e ContainerEvent
     */
    public void componentAdded(ContainerEvent e) {
        ((Container) e.getSource()).validate();
    }

    /**
     * This method is implemented by the ContainerListener and used to redraw
     * the container content if this has been changed.
     *
     * @param e ContainerEvent
     */
    public void componentRemoved(ContainerEvent e) {
        ((Container) e.getSource()).validate();
    }

    /**
     * This method may be used to change the application icon shown in
     * the taskbar as well as the title bar of the application. As
     * parameter an <code>ImageIcon</code> object is expected.
     *
     * @param icon The <code>ImageIcon</code> object defining the icon for the application displayed in the title bar of the application frame
     */
    @SuppressWarnings("BusyWait")
    public void setApplicationIcon(ImageIcon icon) {
        Image image = icon.getImage();
        while (!getToolkit().prepareImage(image, -1, -1, this)) {
            try {
                Thread.sleep(100);
            } catch (Exception ignored) {
            }
        }
        setIconImage(image);
    }

    /**
     * This function returns a reference of the
     * statusbar used in the application window
     *
     * @return A reference to the statusbar
     */
    public JApplicationStatusbar getStatusBar() {
        return JApplicationStatusbar.getInstance();
    }

    /**
     * Invoke this method to perform a clean shutdown of the
     * application. Using this method all custom settings made
     * during the application run are stored in the user properties.
     *
     * @param state The applications exit state
     */
    public void exit(int state) {
        System.exit(state);
    }
}

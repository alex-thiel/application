package de.alx_development.application.example;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Bootstrapper;
import de.alx_development.application.JApplicationFrame;
import de.alx_development.filesystem.DirectoryTree;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Logger;

public class HelloWorld extends JApplicationFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6551561462173856818L;

	protected Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * The main function which is invoked if the application is started.
	 * The args parameter contains the supplied command-line arguments as an array of String objects.
	 * @param args command-line arguments as an array of Strings
	 */
	public static void main(String[] args)
	{
		Bootstrapper.load(HelloWorld.class);
	}
	
	public HelloWorld()
	{
		super();

		// Using a tabbed pane to visualize the component examples
		JTabbedPane jTabbedPane = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT );

		TextArea jTextArea = new TextArea();

		jTabbedPane.addTab("Information", jTextArea);

		DirectoryTree directoryTree = new DirectoryTree();
		directoryTree.setRoot(new File(System.getProperty("user.home")));

		JScrollPane directoryPane = new JScrollPane(directoryTree);
		directoryPane.setMinimumSize(new Dimension(250, 20));
		directoryPane.setPreferredSize(new Dimension(450, 100));

		JSplitPane splitPaneVert1;
		splitPaneVert1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, directoryPane, jTabbedPane);
		splitPaneVert1.setOneTouchExpandable(true);

		add(splitPaneVert1, BorderLayout.CENTER);
	}

	private class TextArea  extends JPanel {
		TextArea() {
			super();

			logger = Logger.getLogger(getClass().getName());
			logger.fine("Building textarea and reading content");

			JScrollPane jScrollPane = new JScrollPane();
			JTextPane jTextPane = new JTextPane();

			// Setting up the stylesheet
			HTMLEditorKit editorKit = new HTMLEditorKit();
			StyleSheet defaultStyle = editorKit.getStyleSheet();
			StyleSheet style = new StyleSheet();
			style.addStyleSheet(defaultStyle);
			editorKit.setStyleSheet(style);

			// Setting up the JTextPane
			jTextPane.setEditable(false);
			jTextPane.setOpaque(true);
			jTextPane.setEditorKit(editorKit);

			// Setting up the enclosing scroll pane
			jScrollPane.setBounds(new Rectangle(40, 30, 150, 50));
			jScrollPane.getViewport().add(jTextPane, null);

			// Adding all to this panel
			setLayout(new BorderLayout());
			add(jScrollPane, BorderLayout.CENTER);

			logger.fine("Loading the textarea content from resource");
			String content = new Scanner(getClass().getResourceAsStream("HelloWorld.html"), StandardCharsets.UTF_8).useDelimiter("\\A").next();
			jTextPane.setText(content);

			logger.fine("Loading resource finished");
		}
	}
}

package de.alx_development.application.logging;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.logging.ErrorManager;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import de.alx_development.application.SplashScreen;

public class SplashScreenLogHandler extends Handler
{
	private SplashScreen splashscreen;
	
	/**
	 * The <code>SplashScreenLogHandler</code> is used to handle to log messages
	 * which should be displayed in the splash screen during the application initialization.
	 */
	public SplashScreenLogHandler(SplashScreen splashscreen)
	{
		super();
		this.splashscreen = splashscreen;
		setLevel(Level.INFO);
		
		Formatter formatter = new SimpleMessageFormatter();
		setFormatter(formatter);
		
		Logger.getLogger("").addHandler(this);
	}

	/**
	 * Close the Handler and free all associated resources.
	 * The close method will perform a flush and then close the Handler.
	 * After close has been called this Handler should no longer be used.
	 * Method calls may either be silently ignored or may throw runtime exceptions. 
	 */
	@Override
	public void close() throws SecurityException
	{
		flush();
		splashscreen = null;
	}

	/**
	 * Flush any buffered output.
	 */
	@Override
	public void flush()
	{
		// TODO Auto-generated method stub
	}

	/**
	 * Publish a <code>LogRecord</code>.
	 * The logging request was made initially to a <code>Logger</code> object,
	 * which initialized the <code>LogRecord</code> and forwarded it here.
	 * The Handler is responsible for formatting the message,
	 * when and if necessary. The formatting should include localization.
	 * 
	 * @param record Description of the log event
	 */
	@Override
	public void publish(LogRecord record)
	{
		String message;
		if (isLoggable(record))
		{
			try
			{
				message = getFormatter().format(record);
			}
			catch (Exception e)
			{
				reportError(null, e, ErrorManager.FORMAT_FAILURE);
				return;
			}
			try
			{
				splashscreen.setText(message);
			}
			catch (Exception e)
			{
				reportError(null, e, ErrorManager.WRITE_FAILURE);
			}
		}
	}
}

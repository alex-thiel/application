package de.alx_development.application.languages;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class Translator
{
	private static Translator instance;
	
	private ResourceBundle localLanguages;
	private ResourceBundle internalLanguages;
	
	/**
	 * Use this function to get a handle to the local <code>Translator</code>
	 * object which handles all translation tasks for the running application.
	 * 
	 * @return A reference to the <code>Translator</code>
	 */
	public static Translator getInstance()
	{
		if(instance == null)
			instance = new Translator();
		
		return instance;
	}
	
	/**
	 * The <code>Translator</code> is implemented as singleton to ensure access from
	 * every object to the translation database. Use the <code>getInstance</code>
	 * method to get a reference to the instance of the translator.
	 * 
	 * @see #getInstance()
	 */
	private Translator()
	{
		de.alx_development.application.Application.logger.log(Level.FINE, "Initializing Translator");
		
		/*
		 * Loading the translations.
		 */
		try
		{
			internalLanguages = ResourceBundle.getBundle("de/alx_development/application/languages/languages");
			localLanguages = ResourceBundle.getBundle("languages");
		}
		catch(MissingResourceException exc)
		{
			de.alx_development.application.Application.logger.log(Level.CONFIG, exc.getMessage()+" using default.");
		}
		
		de.alx_development.application.Application.logger.log(Level.FINE, "Translator initialized and available");
	}
	
	/**
	 * This method can be used to get access to the translation database
	 * for all programs in this archive.
	 * 
	 * @param key The key for the translation
	 * 
	 * @return The localized string for the requested key
	 */
	public String getLocalizedString(String key)
	{
		String returnValue = key;
		try
		{
			returnValue = localLanguages.getString(key);
		}
		catch(Exception e) //Missing local resource, try internal
		{
			try
			{
				returnValue = internalLanguages.getString(key);
			}
			catch(Exception exc)
			{
				de.alx_development.application.Application.logger.log(Level.FINE, "Language translation failure: No translation found for \""+key+"\"");
			}
		}
		return returnValue;
	}
}

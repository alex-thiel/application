/*
 * DirectoryTreeModel.java
 *
 * Created on 5. April 2003
 * (c) ALX-Development
 */

package de.alx_development.filesystem;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

/**
 * This class provides a simple tree model for handling
 * directory structures.
 * 
 * @author  Alexander Thiel
 */
public class DirectoryTreeModel implements javax.swing.tree.TreeModel
{
	/** List of listeners */
    protected EventListenerList listenerList = new EventListenerList();
    
	private File root; //The root file/directory
	private FilenameFilter filter = new FilenameFilter()
									{
										public boolean accept(File f, String name)
										{
											return !(f.isHidden() || name.startsWith("."));
										}
									};
	
	/**
	 * Constructor without a filename filter and an
	 * empty structure.
	 */
	public DirectoryTreeModel() {}
	
	/**
	 * Constructor for unfiltered directory structures.
	 * 
	 * @param root The root file/directory
	 */	
	public DirectoryTreeModel(File root)
	{
		setRoot(root);
	}
	
	/**
	 * Constructor for filtered directory structures. The filter is implemented
	 * as FilenameFilter as provided in java.io.FilenameFilter.
	 * 
	 * @param root The root file/directory
	 * @param filter The Filename filter
	 * 
	 * @see java.io.FilenameFilter
	 */		
	public DirectoryTreeModel(File root, FilenameFilter filter)
	{
		setRoot(root);
		setFilenameFilter(filter);
	}
	
	/**
	 * This method changes the root for the directory model.
	 * All TreeModelListener are notified that the structure has
	 * changed.
	 * 
	 * @param root
	 */
	public void setRoot(File root)
	{
		File oldRoot = this.root;
		this.root = root;
		if (root == null && oldRoot != null)
		{
			fireTreeStructureChanged(this, null);
		}
		else
		{
			nodeStructureChanged(root);
		}
	}

	/**
	 * This method changes the filename filter for the directory model.
	 * All TreeModelListener are notified that the structure has
	 * changed.
	 * A <code>null</code> value will create a filter which passes all
	 * files, hidden ones included.
	 * 
	 * @param filter
	 */
	public void setFilenameFilter(FilenameFilter filter)
	{
		if(filter == null)
		{
			// creating a Dummyfilter
			filter = new FilenameFilter()
					{
						public boolean accept(File f, String name)
						{
							return true;
						}
					};
		}
		this.filter = filter;
		nodeStructureChanged(root);
	}
		
	/**
	 * This method returns the child object as Object for the given parent
	 * at the specified index.
	 * 
	 * @param parent The parent node
	 * @param index Index of the childnode
	 */
	public Object getChild(Object parent, int index)
	{
		if (parent instanceof File)
		{
			File f = (File) parent;
			return f.listFiles(filter)[index];
		}
		return null;
	}
	
	/**
	 * Returns the number of childs of the given parent node
	 * object as integer value.
	 * 
	 * @param node The node which childs should be counted
	 * 
	 * @return Number of childs
	 */
	public int getChildCount(Object node)
	{
		if (node instanceof File)
		{
			File f = (File) node;
			if (f.isDirectory()) return f.listFiles(filter).length;
		}
		return 0;
	}
	
	/**
	 * This method may be used to determine which index the given
	 * child has got in the parent node. If the given child object isn't
	 * a child object of the parent node -1 is returned.
	 * 
	 * @param parent The parent node
	 * @param child The searched childnode
	 * 
	 * @return Index of the child inbetween the parent
	 */
	public int getIndexOfChild(Object parent, Object child)
	{
		File d = (File) parent;
		File[] f = d.listFiles(filter);
		if (f == null) return -1;
		for (int i = 0; i < f.length; i++)
		{
			if (f[i] == child) return i;
		}
		return -1;
	}
	
	/**
	 * This method returns the root node for the structure.
	 * 
	 * @return The root node.
	 */
	public Object getRoot()
	{
		return root;
	}
	
	/**
	 * This method returns true, if the node is a leaf node.
	 * 
	 * @param node
	 * @return True, if the node is a leaf node
	 */
	public boolean isLeaf(Object node)
	{
		File f = (File) node;
		return f.isFile();
	}

	/**
	 * Builds the parents of node up to and including the root node,
	 * where the original node is the last element in the returned array.
	 * The length of the returned array gives the node's depth in the
	 * tree.
	 * 
	 * @param aNode the TreeNode to get the path for
	 */
	public File[] getPathToRoot(File aNode)
	{
		return getPathToRoot(aNode, 0);
	}
	
	/**
	 * Builds the parents of node up to and including the root node,
	 * where the original node is the last element in the returned array.
	 * The length of the returned array gives the node's depth in the
	 * tree.
	 * 
	 * @param aNode  the TreeNode to get the path for
	 * @param depth  an int giving the number of steps already taken towards
	 *        the root (on recursive calls), used to size the returned array
	 * @return an array of TreeNodes giving the path from the root to the
	 *         specified node 
	 */
	protected File[] getPathToRoot(File aNode, int depth)
	{
		File[] retNodes;
		// This method recurses, traversing towards the root in order
		// size the array. On the way back, it fills in the nodes,
		// starting from the root and working back to the original node.

		/* 
		 * Check for null, in case someone passed in a null node, or
		 * they passed in an element that isn't rooted at root.
		 */
		if(aNode == null)
		{
			if(depth == 0)
			{
				return null;
			}
			else
			{
				retNodes = new File[depth];
			}
		}
		else
		{
			depth++;
			if(aNode == root)
			{
				retNodes = new File[depth];
			} 
			else
			{
				retNodes = getPathToRoot(new File(aNode.getParent()), depth);
			} 
			retNodes[retNodes.length - depth] = aNode;
		}
		return retNodes;
	}

	/**
	  * Invoke this method if you've totally changed the children of
	  * node and its childrens children...  This will post a
	  * treeStructureChanged event.
	  */
	public void nodeStructureChanged(File node)
	{
		if(node != null)
		{
		   fireTreeStructureChanged(this, getPathToRoot(node), null, null);
		}
	}
	
	/**
	 * Notifies all listeners that have registered interest for
	 * notification on this event type.  The event instance 
	 * is lazily created using the parameters passed into 
	 * the fire method.
	 */
	private void fireTreeStructureChanged(Object source, TreePath path)
	{
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Lazily create the event:
		TreeModelEvent e = new TreeModelEvent(source, path);
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length-2; i>=0; i-=2)
		{
			((TreeModelListener)listeners[i+1]).treeStructureChanged(e);
		}
	}

	/**
	 * Notifies all listeners that have registered interest for
	 * notification on this event type.  The event instance 
	 * is lazily created using the parameters passed into 
	 * the fire method.
	 *
	 * @param source the node where the tree model has changed
	 * @param path the path to the root node
	 * @param childIndices the indices of the affected elements
	 * @param children the affected elements
	 * @see EventListenerList
	 */
	protected void fireTreeStructureChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length-2; i>=0; i-=2)
		{
			((TreeModelListener)listeners[i+1]).treeStructureChanged(e);      
		}
	}
	
	/**
	 * This model isn't producing any events at present, so
	 * this method is empty.
	 * 
	 * @param treeModelListener
	 */
	public synchronized void addTreeModelListener(TreeModelListener treeModelListener)
	{
		listenerList.add(TreeModelListener.class, treeModelListener);
	}
	
	/**
	 * This model isn't producing any events at present, so
	 * this method is empty.
	 * 
	 * @param treeModelListener
	 */
	public synchronized void removeTreeModelListener(TreeModelListener treeModelListener)
	{
		listenerList.remove(TreeModelListener.class, treeModelListener);
	}

	/**
	 * Returns an array of all the tree model listeners
	 * registered on this model.
	 *
	 * @return all of this model's <code>TreeModelListener</code>s
	 *         or an empty
	 *         array if no tree model listeners are currently registered
	 *
	 * @see #addTreeModelListener
	 * @see #removeTreeModelListener
	 */
	public TreeModelListener[] getTreeModelListeners()
	{
		return (TreeModelListener[])listenerList.getListeners(TreeModelListener.class);
	}
	
	/**
	 * This model isn't producing any events at present, so
	 * this method is empty.
	 * 
	 * @param treePath
	 * @param obj
	 */
	public void valueForPathChanged(javax.swing.tree.TreePath treePath, Object obj) {}
}

package de.alx_development.filesystem.observer;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.io.File;

/**
 * This event object is delivered if a change on
 * a file has been detected.
 * 
 * @author alex
 *
 */
public class FileChangeEvent
{
	private File file;
	private int event;
	private Observer source;
	
	/**
	 * Default constructor which expects the file, the eventcode and source
	 * as parameter.
	 * 
	 * @param file The file which has been changed
	 * @param event The eventcode from the observer
	 * @param source A reference to the observer which has detected the change
	 */
	protected FileChangeEvent(File file, int event, Observer source)
	{
		super();
		this.source = source;
		this.file = file;
		this.event = event;
	}

	/**
	 * Use this function to get the event code as defined by the <code>observer</code>
	 * 
	 * @return the event
	 */
	public int getEvent()
	{
		return event;
	}

	/**
	 * The file which has been modified is referenced by the returned file object.
	 * Be carefull with delete events. In this case the file is not longer
	 * existing.
	 * 
	 * @return the file
	 */
	public File getFile()
	{
		return file;
	}

	/**
	 * This function returns a reference to the observer which has detected the file
	 * change event.
	 * 
	 * @return the source
	 */
	public Observer getSource()
	{
		return source;
	}
}

package de.alx_development.filesystem.observer;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.event.EventListenerList;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;

public abstract class Observer implements JNotifyListener
{
	public static final int FILE_CREATED = JNotify.FILE_CREATED;
	public static final int FILE_DELETED = JNotify.FILE_DELETED;
	public static final int FILE_MODIFIED = JNotify.FILE_MODIFIED;
	public static final int FILE_RENAMED = JNotify.FILE_RENAMED;
	
	protected EventListenerList listenerList = new EventListenerList();
	
	private File path;
	private boolean subtreeWatched;
	private Integer watchID;
	
	/**
	 * The default constructor expects the path name which should be
	 * observed and a boolean value which specifies if the subnodes
	 * have also be observed or not.
	 * 
	 * @param path
	 * @param watchSubtree
	 */
	public Observer(String path, boolean watchSubtree) throws FileNotFoundException
	{
		this.path = new File(path);
		this.subtreeWatched = watchSubtree;
		
		if(this.path.isFile())
			this.path = this.path.getParentFile();
		
		if(!this.path.exists())
			throw new FileNotFoundException("Defined file/folder doesn't exist");
	}
	
	/**
	 * Use this method to activate/deactivate the observer.
	 * This should be done before loosing a handle to the object to
	 * avoid running zombies in the background.
	 * 
	 * @param state
	 * @throws ObserverException
	 */
	public void setActive(boolean state) throws ObserverException
	{
		if(state == false && watchID != null)
		{
			try
			{
				JNotify.removeWatch(watchID.intValue());
				this.watchID = null;
			} 
			catch (JNotifyException e)
			{
				throw new ObserverException("Failed to deactivate observer: "+e.getLocalizedMessage());
			}
		}
		
		if(state == true && watchID == null)
		{
			try
			{
				this.watchID = (Integer) JNotify.addWatch(getPath().getAbsolutePath(), getObserveMask(), isSubtreeWatched(), this);
			}
			catch (JNotifyException e)
			{
				throw new ObserverException("Failed to activate observer: "+e.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * This function returns true, if the observer is currently activated,
	 * otherwise false.
	 * 
	 * @return true is observer is active
	 */
	public boolean isRunning()
	{
		if(watchID != null)
			return true;
		return false;
	}
	
	/**
	 * If the garbage collector removes an object this overridden
	 * method ensures the underlying observer process is down.
	 */
	public void finalize()
	{
		try
		{
			this.setActive(false);
		} catch (ObserverException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Internal method which is triggered by the native filesystem
	 * observer if a new file create event has been detected on the
	 * observed path
	 */
	public void fileCreated(int arg0, String path, String file)
	{
		if(isValid(path, file))
			fireFileChangeEvent(path, file, FILE_CREATED);
	}

	/**
	 * Internal method which is triggered by the native filesystem
	 * observer if a new file delete event has been detected on the
	 * observed path
	 */
	public void fileDeleted(int arg0, String path, String file)
	{
		if(isValid(path, file))
			fireFileChangeEvent(path, file, FILE_DELETED);
	}

	/**
	 * Internal method which is triggered by the native filesystem
	 * observer if a new file modiefied event has been detected on the
	 * observed path
	 */
	public void fileModified(int arg0, String path, String file)
	{
		if(isValid(path, file))
			fireFileChangeEvent(path, file, FILE_MODIFIED);
	}

	/**
	 * Internal method which is triggered by the native filesystem
	 * observer if a new file renamed event has been detected on the
	 * observed path
	 * The filter is asked using the old filename. The process instead
	 * is triggered on the new file.
	 */
	public void fileRenamed(int arg0, String path, String file, String newfile)
	{
		if(isValid(path, file))
			fireFileChangeEvent(path, newfile, FILE_RENAMED);
	}

	/**
	 * This function returns the root path which this observer is
	 * watching and waiting for events. Depending on the setting for
	 * the subtree watching all underlying subnodes are also observed.
	 * 
	 * @return the path
	 * 
	 * @see #isSubtreeWatched()
	 */
	public File getPath()
	{
		return path;
	}

	/**
	 * This function returns true, if the observer is also acting if a
	 * filesystem change event on any of the roots subnodes has been fired.
	 * 
	 * @return the subtreeWatched
	 * 
	 * @see #getPath()
	 */
	public boolean isSubtreeWatched()
	{
		return subtreeWatched;
	}
	
	/**
	 * Override this method if the subclass should only observe some
	 * special events like modification or deletion. By default all
	 * events are handled.
	 * 
	 * @return The integer mask for the filesystem observation
	 */
	public int getObserveMask()
	{
		return JNotify.FILE_ANY;
	}
	
	/**
	 * Use this method to register a listener which is interrested in being
	 * notified about changes on observed files.
	 * 
	 * @param handler
	 */
	public synchronized void addFileChangeHandler(FileChangeHandler handler)
	{
		listenerList.add(FileChangeHandler.class, handler);
	}
	
	/**
	 * Use this method to remove a previous registered change listener
	 * from this observer object.
	 * 
	 * @param handler
	 */
	public synchronized void removeFileChangeHandler(FileChangeHandler handler)
	{
		listenerList.remove(FileChangeHandler.class, handler);
	}
	
	/**
	 * This function returns all <code>Handler</code> object registered
	 * as listeners to this observer.
	 * 
	 * @return An array of handlers which listens to changes detected by this observer
	 */
	public FileChangeHandler[] getFileChangeHandler()
	{
		FileChangeHandler[] listeners = listenerList.getListeners(FileChangeHandler.class);
		return listeners;
	}
	
	/**
	 * Internaly used method which is triggered if a change to an observed file has been
	 * occured to inform all registered handlers.
	 */
	private void fireFileChangeEvent(String path, String file, int eventcode)
	{
		File f = new File(path+"/"+file);
		FileChangeHandler[] listeners = getFileChangeHandler();
		FileChangeEvent event = new FileChangeEvent(f, eventcode, this);
		for(int i = listeners.length-1; i>=0; i-=1)
		{
			listeners[i].process(event);
		}
	}
	
	/**
	 * This function should return true, if the detected file should be
	 * controlled by this observer, otherwise this returns false and any
	 * further operation is canceled.
	 * 
	 * @param path The path to the file
	 * @param file The filename
	 * @return true, if file should be processed
	 */
	public abstract boolean isValid(String path, String file);
}

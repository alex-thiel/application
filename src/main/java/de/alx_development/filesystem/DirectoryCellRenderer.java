/*
 * DirectoryCellRenderer.java
 *
 * Created on 5. April 2003
 * ALX-Development
 */

package de.alx_development.filesystem;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.filechooser.*;
import java.io.*;
import java.util.*;

/**
 * DirectoryCellRenderer should be used to get a nice styled
 * view for the DirectoryTree. If this is be used on a Microsoft Windows
 * platform. The system specific icons are displayed.
 * 
 * @author  Alexander Thiel
 */
public class DirectoryCellRenderer extends DefaultTreeCellRenderer
{
	private static final long serialVersionUID = 1L;
	private static final ImageIcon closed_folder = new ImageIcon(DirectoryCellRenderer.class.getResource("closed_folder.gif"));
	private static final ImageIcon open_folder = new ImageIcon(DirectoryCellRenderer.class.getResource("open_folder.gif"));
	private static Properties filetypes = new Properties();
	private FileSystemView fileSystemView = FileSystemView.getFileSystemView();

	static
	{
		/*
		 * Loading the translations.
		 */
		try
		{
			filetypes.load(DirectoryCellRenderer.class.getResourceAsStream("filetypes/filetypes.properties"));			
		}
		catch(Exception exc)
		{
			System.out.println("WARNING: " + exc.getMessage());
		}
	}
	/**
	 * The constructor of the cell renderer
	 */
	public DirectoryCellRenderer()
	{
		super();
	}
	
	/**
	 * This is the sole method of this object. It passes the
	 * given parameters to the wrapped TreeCellRenderer.
	 * 
	 * @param tree JTree Object which should be formatted.
	 * @param value The node to format
	 * @param selected True, if the node is selected
	 * @param expanded True, if the node is expanded
	 * @param leaf True, if this is a leaf node.
	 * @param row
	 * @param hasFocus True, if the node has got the focus.
	 * 
	 * @return Returns a TreeCellRendererComponent
	 */
	public java.awt.Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
							boolean leaf, int row, boolean hasFocus)
	{
		super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

		/*
		 * For directories we use our own icons
		 * instead the system ones.
		 */
		if(((File)value).isDirectory())
		{
			if(expanded){ setIcon(open_folder); }
			else { setIcon(closed_folder); } 
		}
		else
		{
			if(System.getProperty("os.name").matches("(Win|win).*"))
			{
				/*
				 * Java on Microsoft Windows systems supports
				 * the usage of system icons.
				 */
				setIcon(fileSystemView.getSystemIcon((File)value));
			}
			else
			{
				/*
				 * Using the file extension to determine the
				 * file type for the icon.
				 */
				String filename = ((File)value).getName();
				int lastDot = filename.lastIndexOf('.');
				String extension = new String();
				if(lastDot > 0)
				{
					extension =  filename.substring(lastDot + 1);
				}
				setIcon(new ImageIcon(DirectoryCellRenderer.class.getResource("filetypes/" + filetypes.getProperty(extension, "misc.png"))));			
			}
		}
		
		setText(fileSystemView.getSystemDisplayName((File)value));
		return this;
	}
}

/*
 * DirectoryTreeBeanInfo.java
 * Created on 06.01.2004
 *
 * (c) ALX-Development
 */
package de.alx_development.filesystem;

/*-
 * #%L
 * application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.beans.*;
import java.awt.Image;

/**
 * This class provides additional JavaBean information for
 * the DirectoryTree class.
 * It is used by graphical development environments to display
 * the correct informations and graphics.
 * 
 * @author Alexander Thiel
 */
public class DirectoryTreeBeanInfo extends SimpleBeanInfo
{
	/**
	 * This method returns the image to display as icon
	 * in the graphical environment.
	 * 
	 * @param iconKind The requested icon format
	 */
	public Image getIcon(int iconKind)
	{
		if(iconKind == BeanInfo.ICON_COLOR_16x16)
		{
			return loadImage("DirectoryTree_COLOR_16x16.gif");
		}
		if(iconKind == BeanInfo.ICON_COLOR_32x32)
		{
			return loadImage("DirectoryTree_COLOR_32x32.gif");
		}
		if(iconKind == BeanInfo.ICON_MONO_16x16)
		{
			return loadImage("DirectoryTree_MONO_16x16.gif");
		}
		if(iconKind == BeanInfo.ICON_MONO_32x32)
		{
			return loadImage("DirectoryTree_MONO_32x32.gif");
		}
		return null;
	}
}

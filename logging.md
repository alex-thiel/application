#LOG Level definition for the application framework

It is often discussed and there are several explanations available in the internet which often are nit in sync in detail. To avoid misunderstanding this document declares the log levels as used for the application framework and which shall be used by projects depending on this library.

##SEVERE

One or more functionalities are not working, preventing some functionalities from working correctly or the whole system doesn’t fulfill it's business functionalities.

It is the log level that tells that the application encountered an event or entered a state in which one of the crucial business functionality is no longer working. A SEVERE log level may be used when the application is not able to connect to a crucial data store like a database or all the payment systems are not available and users can’t checkout their baskets in your e-commerce.

##WARNING

Unexpected behavior happened inside the application, but it is continuing its work and the key business features are operating as expected.

It's the log level that indicates that something unexpected happened in the application, a problem, or a situation that might disturb one of the processes. But that doesn’t mean that the application failed. The WARNING level should be used in situations that are unexpected, but the code can continue the work. For example, a parsing error occurred that resulted in a certain document not being processed.

#INFO

An event happened, the event is purely informative and can be ignored during normal operations.

This is the standard log level indicating that something happened, application entered a     certain state, etc. For example, a controller of your authorization API may include an INFO log level with information on which user requested authorization if the authorization was successful or not. The information logged using the INFO log level should be purely informative and not looking into them on a regular basis shouldn’t result in missing any important information.

##CONFIG

This log level should be used for anything related to configuration issues except errors with the SEVERE log level.

##FINE

A log level used for events considered to be useful during software debugging when more granular information is needed.

It is less granular compared to the FINER level, but it is more than you will need in everyday use. The FINE log level should be used for information that may be needed for diagnosing issues and troubleshooting or when running application in the test environment for the purpose of making sure everything is running correctly


##FINER

A log level describing events showing step by step execution of your code that can be ignored during the standard operation, but may be useful during extended debugging sessions.

One of the most fine-grained information only used in cases where you need the full visibility of what is happening in your application and inside the third-party libraries that you use. You can expect the FINER logging level to be very verbose. You can use it for example to annotate each step in the algorithm or each individual query with parameters in your code.

##FINEST

Like the FINER level this log level is also describing events showing step by step execution of your code that can be ignored during the standard operation.

It should be used in librabries which are used by the application to have a debug level for libs and application seperated.